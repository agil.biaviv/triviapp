import React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'

// screens
import SignIn from './screens/LoginScreen'
import Register from './screens/RegisterScreen'
import Home from './screens/HomeScreen'
import Score from './screens/ScoresScreen'
import Leaderboard from './screens/LeaderboardScreen'
import Quiz from './screens/QuizScreen'
import QuizResult from './screens/QuizResultScreen';

import { createStore } from 'redux'
import { Provider } from 'react-redux'
import allReducers from './store/reducers';

const store = createStore(allReducers)

const AuthStack = createStackNavigator()

const TabStack = createBottomTabNavigator()
const tabs = () => (
  <TabStack.Navigator initialRouteName="home"
    tabBarOptions={{
      activeTintColor: "black",
    }}
  >
    <TabStack.Screen component={Score}
      name="score"
      options={{
        tabBarLabel: "Statistics",
        tabBarIcon: ({size, color}) => (
          <MaterialIcon name="account-circle-outline" size={size} color={color}/>
        )
      }}
    />
    <TabStack.Screen component={Home}
      name="home"
      options={{
        tabBarLabel: "Trivia",
        tabBarIcon: ({color, size}) => (
          <MaterialIcon name="layers-triple-outline" size={size} color={color} />
        ),
      }} />
    <TabStack.Screen component={Leaderboard}
      name="leaderboard"
      options={{
        tabBarLabel: "Leaderboard",
        tabBarIcon: ({color, size}) => (
          <MaterialIcon name="trophy-variant-outline" size={size} color={color} />
        )
      }}
    />
  </TabStack.Navigator>
)

const MainApp = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <AuthStack.Navigator initialRouteName="signin">
          <AuthStack.Screen name="signin" component={SignIn} options={{headerShown: false}}/>
          <AuthStack.Screen name="register" component={Register}
            options={{
              headerTitle: '',
              headerBackTitleVisible: false,
              headerTransparent: true,
              headerTintColor: "white",
            }}
          />
          <AuthStack.Screen name="home" component={tabs}
          options={{
            headerShown: false
          }} />

          <AuthStack.Screen component={Quiz} name="quiz" options={{headerShown: false}}/>
          <AuthStack.Screen component={QuizResult} name="quizResult" options={{headerShown: false}}/>

        </AuthStack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default MainApp;