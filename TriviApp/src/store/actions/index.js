export const auth = (data) => {
    return {
        type: "AUTH",
        payload: data
    }
}

export const nextQuestion = () => {
    return {
        type: "NEXT"
    }
}

export const resetQuestion = () => {
    return {
        type: "RESET"
    }
}

export const calculateScore = () => {
    return {
        type: "CALCULATE"
    }
}

export const resetScore = () => {
    return {
        type: "RESETSCORE"
    }
}

export const addTriviaPoints = (tempScore) => {
    return {
        type: 'ADD_TRIVIAPOINTS',
        payload: tempScore
    }
}

export const addAnimalPoints = (tempScore) => {
    return {
        type: 'ADD_ANIMALPOINTS',
        payload: tempScore
    }
}

export const addComputerPoints = (tempScore) => {
    return {
        type: 'ADD_COMPUTERPOINTS',
        payload: tempScore
    }
}

export const addMythPoints = (tempScore) => {
    return {
        type: 'ADD_MYTHPOINTS',
        payload: tempScore
    }
}