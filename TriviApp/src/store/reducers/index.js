import authReducer from './auth'
import {
    tempScore,
    triviaPoints,
    animalPoints,
    computerPoints,
    mythPoints
} from './score'
import questionNumber from './questionNumber'

import { combineReducers } from 'redux'

const allReducers = combineReducers({
    authReducer,
    tempScore,
    animalPoints,
    computerPoints,
    mythPoints,
    triviaPoints,
    questionNumber
})

export default allReducers