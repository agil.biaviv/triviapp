import { resetScore } from "../actions"

export const tempScore = (state = 0, action) => {
  switch(action.type) {
    case "CALCULATE" :
      return state + 10
    case "RESETSCORE" :
      return 0
    default :
      return state
  }
}

export const triviaPoints = (state = 0, action) => {
  switch(action.type) {
    case "ADD_TRIVIAPOINTS":
      return state + action.payload
    default :
      return state
  }
}

export const animalPoints = (state = 0, action) => {
  switch(action.type) {
    case "ADD_ANIMALPOINTS":
      return state + action.payload
    default:
      return state
  }
}

export const computerPoints = (state = 0, action) => {
  switch(action.type) {
    case "ADD_COMPUTERPOINTS":
      return state + action.payload
    default:
      return state
  }
}
export const mythPoints = (state = 0, action) => {
  switch(action.type) {
    case "ADD_MYTHPOINTS":
      return state + action.payload
    default:
      return state
  }
}
