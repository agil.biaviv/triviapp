import React from 'react';
import HTML from 'react-native-render-html'

const questions = (props) => {
  const question = `<p class="question">${props.data.question}</p>`
  return (
    <HTML html={question} classesStyles={{question : {color: secondaryColor, textAlign: "center", fontSize: 14}}}/>
  );
}

const secondaryColor = "white"

export default questions;