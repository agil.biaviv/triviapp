import React, { useState } from 'react';

import { TouchableHighlight } from 'react-native-gesture-handler';
import { StyleSheet, Text } from 'react-native';

import {useDispatch ,useSelector} from 'react-redux'
import { nextQuestion, resetQuestion, calculateScore, resetScore, addTriviaPoints } from '../store/actions'

import HTML from 'react-native-render-html'

const AnswerOption = (props) => {
  const [isChecked, setIsChecked] = useState(true)
  const category = props.category

  const questionIndex = useSelector(state => state.questionNumber)

  const score = useSelector(state => state.tempScore)

  const dispatch = useDispatch()

  const answerKey = props.answerKey
  const isCorrect = (props.data == answerKey) ? true : false

  const option = `<p class="option">${props.data}</p>`
  return (
    <TouchableHighlight style={{...styles.container, backgroundColor: isChecked ? primaryColor : secondaryColor }}
    onPress={() => {
      if(questionIndex == 9) {
        dispatch(category(score))
        dispatch(addTriviaPoints(score))
        dispatch(resetQuestion())
        return props.navigation.navigate('quizResult')
      }
      dispatch(nextQuestion())
      console.log(questionIndex)
      console.log( "Score : "+  score)
      props.nextHandler(questionIndex)
      if(isCorrect) {
        dispatch(calculateScore())
      }
    }}
    >
      <HTML html={option} classesStyles={{option : {color: secondaryColor, textAlign: "center", fontSize: 14}}}/>
    </TouchableHighlight>
   );
}

export default AnswerOption;



const primaryColor = "black"
const secondaryColor = "white"

const styles = StyleSheet.create({
  container: {
    height: 50,
    marginHorizontal: 20,
    borderRadius: 15,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: primaryColor,
    justifyContent: "center",
    marginBottom: 10
  },
  option: {
  }

})