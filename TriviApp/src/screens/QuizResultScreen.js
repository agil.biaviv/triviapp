import React, { useState } from 'react';
import { View, Text, Button, StatusBar, StyleSheet } from 'react-native';

import { useSelector, useDispatch } from 'react-redux'
import { TouchableHighlight, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { resetScore } from '../store/actions';

const QuizResultScreen = (props) => {
  const [correctAnswer, setCorrectAnswer] = useState(0)

  const score = useSelector(state => state.tempScore)
  const dispatch = useDispatch()
  return (
    <>
      <StatusBar translucent={false}/>
      <View style={styles.bgTop} >
        <View style={styles.topContent}></View>
      </View>
      <View style={styles.bgBottom} >
        <View style={styles.bottomContent}></View>
      </View>

      <View style={styles.container}>
          <Text style={{...styles.topText, fontSize: 22}}>Well done!</Text>
          <Text style={{...styles.topText, fontSize: 36}}>Your Score</Text>
          <View style={{flex: 1, marginTop: 50, alignItems: "center"}}>
            <Text style={{textAlign: "center", fontSize: 100, fontWeight: "bold"}}>{score.toString()}</Text>
            <TouchableHighlight style={styles.btn}
              onPress={() => {
                dispatch(resetScore())
                props.navigation.navigate('leaderboard')
              }}
            >
              <Text style={{...styles.btnText, color: secondaryColor}}>Leaderboard</Text>
            </TouchableHighlight>
            <TouchableWithoutFeedback style={{...styles.btn, backgroundColor: secondaryColor}}
              onPress={() => {
                dispatch(resetScore())
                props.navigation.navigate('home')
              }}
            >
              <Text style={styles.btnText}>Select Category</Text>
            </TouchableWithoutFeedback>
          </View>
      </View>
    </>
  );
}

const primaryColor = "black"
const secondaryColor = "white"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    position: "relative",
    zIndex: 0,
  },
  bgTop: {
    backgroundColor: secondaryColor,
    height: 180,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  bgBottom: {
    backgroundColor: primaryColor,
    position: "absolute",
    top: 180,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  topContent: {
    backgroundColor: primaryColor,
    height: 180,
    borderBottomRightRadius: 50,
  },
  bottomContent: {
    flex: 1,
    backgroundColor: secondaryColor,
    borderTopLeftRadius: 50
  },
  topText: {
    color: secondaryColor,
    fontWeight: "bold",
    textAlign: "center",
    marginVertical: 15
  },
  btn: {
    borderRadius: 20,
    width: 175,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: primaryColor,
    marginTop: 30
  },
  btnText : {
    fontSize: 16,
    fontWeight: "bold",
    letterSpacing: .3
  }
})

export default QuizResultScreen;