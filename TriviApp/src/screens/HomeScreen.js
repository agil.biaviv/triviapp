import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Image,
  TouchableHighlight
} from 'react-native';

import {useSelector} from 'react-redux'

import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { TouchableRipple } from 'react-native-paper';

const HomeScreen = (props) => {
  const username = useSelector(state => state.authReducer)
  return (
    <>
      <StatusBar translucent={false}/>
      <View style={styles.bgTop}>
        <View style={styles.topContent}></View>
      </View>
      <View style={styles.bgBottom}>
        <View style={styles.bottomContent}></View>
      </View>

      <View style={styles.container}>
        <View style={styles.navbar}>
          <View style={{ transform: [{rotate: "180deg" }], alignItems: "flex-end"}}>
            <TouchableOpacity onPress={() => {
              props.navigation.navigate('signin')
            }}>
              <MaterialIcon name="logout" size={24} color={secondaryColor}  />
            </TouchableOpacity>

          </View>
          <View style={{flexDirection: "row", alignItems: "center"}}>
            <MaterialIcon name="account-circle" size={24} color={secondaryColor} style={{marginRight: 10}}/>
            <Text style={{color: secondaryColor, fontSize: 16}}>{username}</Text>
          </View>
        </View>


        <Text style={{color: "white", fontSize: 22, padding: 30, marginBottom: 30}}>Test your knowledge and compete with others</Text>

        <View style={{flex: 1}}>
          <Text style={{textAlign: "center", fontSize: 16, fontWeight: "bold", }}>Pick a category to start the trivia quiz</Text>

          <ScrollView style={{marginTop: 30, paddingHorizontal: 15}} horizontal={true}>
            <TouchableHighlight>
              <View style={styles.categoryItem}>
                <TouchableHighlight style={styles.card}
                  onPress={() => props.navigation.navigate('quiz', {
                    categoryId: 27
                  })}
                >
                  <Image source={require('../Images/tiger.jpg')} style={{width: 200, height: 200}} />
                </TouchableHighlight>
                <Text style={styles.categoryName}>Animals</Text>
              </View>
            </TouchableHighlight>
            <View style={styles.categoryItem}>
                <TouchableHighlight style={styles.card}
                  onPress={() => props.navigation.navigate('quiz', {
                    categoryId : 18
                  })}
                >
                  <Image source={require('../Images/network.png')} style={{ width: 300, height: 200,  tintColor: "grey"}} />
                </TouchableHighlight>
              <Text style={styles.categoryName}>Computer Science</Text>
            </View>
            <TouchableHighlight>
              <View style={styles.categoryItem}>
                <TouchableHighlight style={styles.card}
                  onPress={() => props.navigation.navigate('quiz', {
                    categoryId: 20
                  })}
                >
                  <Image source={require('../Images/greek.png')} style={{width: 140, height: 180, marginLeft: 80}} />
                </TouchableHighlight>
                <Text style={styles.categoryName}>Mythology</Text>
              </View>
            </TouchableHighlight>

            <View style={{width: 15, flex: 1, overflow: "hidden"}} ><Text>{/* Padding Right of scroll view */}</Text></View>
          </ScrollView>

        </View>
      </View>
    </>
  );
}

const primaryColor = "black"
const secondaryColor = "white"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    position: "relative",
    zIndex: 0,
  },
  navbar: {
    padding: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  categoryItem: {
    marginRight: 15,
  },
  categoryName: {
    textAlign: "center",
    fontWeight: "bold",
    letterSpacing: .3,
    fontSize: 16,
    marginTop: 5
  },
  card: {
    height: 200,
    backgroundColor: primaryColor,
    width: 200,
    borderRadius: 30,
    elevation: 10,
    overflow: "hidden",
    justifyContent: "flex-end"
  },
  bgTop: {
    backgroundColor: secondaryColor,
    height: 180,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  bgBottom: {
    backgroundColor: primaryColor,
    position: "absolute",
    top: 180,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  topContent: {
    backgroundColor: primaryColor,
    height: 180,
    borderBottomRightRadius: 50,
  },
  bottomContent: {
    flex: 1,
    backgroundColor: secondaryColor,
    borderTopLeftRadius: 50
  },
})
export default HomeScreen;