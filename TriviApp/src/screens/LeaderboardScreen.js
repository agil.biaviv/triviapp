import React, { useState } from 'react';
import { View, Text, StatusBar, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { ScrollView } from 'react-native-gesture-handler';

import { useSelector } from 'react-redux'

const LeaderboardScreen = (props) => {
  const [correctAnswer, setCorrectAnswer] = useState(0)

  return (
    <>
      <StatusBar translucent={false}/>
      <View style={styles.bgTop} >
        <View style={styles.topContent}></View>
      </View>
      <View style={styles.bgBottom} >
        <View style={styles.bottomContent}></View>
      </View>

      <View style={styles.container}>
        <View style={{height: 180, alignItems: "center"}}>
          <Icon name="trophy" size={100} color={secondaryColor}  />
          <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3, marginTop: 15}}>TOP 10 LEADERBOARD OF THIS WEEK</Text>
        </View>
        <ScrollView style={{flex: 1, marginTop: 30}}>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>1</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>{`${useSelector(state => state.authReducer)}`}</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>{`${useSelector(state => state.triviaPoints)}`}</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>2</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>3</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>4</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>5</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>6</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>7</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>8</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>9</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>
          <View style={styles.leaderboardList}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: secondaryColor, marginRight: 20, fontWeight: "bold", fontSize: 20}}>10</Text>
              <Text style={{color: secondaryColor, fontWeight: "bold", letterSpacing: .3}}>-</Text>
            </View>
            <Text style={{color: secondaryColor, fontWeight: "bold", fontSize: 18}}>0</Text>
          </View>



        </ScrollView>
      </View>
    </>
  );
}

const primaryColor = "black"
const secondaryColor = "white"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    position: "relative",
    zIndex: 0,
    paddingHorizontal: 30
  },
  bgTop: {
    backgroundColor: secondaryColor,
    height: 180,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  bgBottom: {
    backgroundColor: primaryColor,
    position: "absolute",
    top: 180,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  topContent: {
    backgroundColor: primaryColor,
    height: 180,
    borderBottomRightRadius: 50,
  },
  bottomContent: {
    flex: 1,
    backgroundColor: secondaryColor,
    borderTopLeftRadius: 50,
  },
  leaderboardList: {
    backgroundColor: primaryColor,
    padding: 20,
    paddingVertical: 15,
    borderRadius: 20,
    marginBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
})

export default LeaderboardScreen;