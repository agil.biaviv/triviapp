import React, { useState } from 'react';
import { View, StyleSheet, Text, StatusBar, TouchableOpacity, TextInput, KeyboardAvoidingView, Dimensions, Platform, Keyboard } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { auth } from '../store/actions'
import { useSelector, useDispatch } from 'react-redux'


const LoginScreen = (props) => {

  const [username, setUsername] = useState("Username")
  const [password, setPassword] = useState("Password")
  const [isPasswordHidden, setIsPasswordHidden] = useState(false)

  const name = useSelector( state => state.authReducer )
  const dispatch = useDispatch()

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <StatusBar style="dark-content" translucent={false} />
      <View style={styles.bgTop}>
          <View style={styles.topContent}></View>
      </View>

      <View style={styles.bgBottom}>
            <View style={styles.bottomContent}></View>
      </View>


      <View style={styles.container}>
        <View style={styles.content}>

          <Icon name="layers-triple-outline" size={80} style={{color: secondaryColor, marginBottom: 50}} />

          <Text style={styles.welcomeText}>Welcome to Triviapp</Text>

          <View style={styles.inputInline}>
            <Icon name="account-outline" size={30} />
            <TextInput style={styles.input}
              selectTextOnFocus={true}
              value={username}
              onChangeText={(text) => setUsername(text)}
            />
          </View>
          <View style={styles.inputInline}>
            <Icon name="lock-outline" size={30} />
            <TextInput style={styles.input}
              value={password}
              selectTextOnFocus={true}
              secureTextEntry={isPasswordHidden}
              onChangeText={(text) => setPassword(text)}
              onFocus={() => setIsPasswordHidden(true)}

            />
          </View>
          <TouchableOpacity style={styles.btn}
            onPress={() => {
              dispatch(auth(username))
              props.navigation.navigate('home')
            }}
          >
            <Text
              style={{...styles.btnText, color: "white"}}
            >SIGN IN</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ marginTop: 40}}
            onPress={() => {props.navigation.push('register')}}
          >
            <Text style={styles.btnText}>Create an account</Text>
          </TouchableOpacity>

        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

let primaryColor = "black"
let secondaryColor = "white"

const {height, width} = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    height: height,
    backgroundColor: "transparent",
  },
  content: {
    justifyContent: "flex-end",
    flex: 1,
    alignItems: "center",
    paddingBottom: 80,
  },
  bgTop: {
    backgroundColor: secondaryColor,
    height: 180,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  bgBottom: {
    backgroundColor: primaryColor,
    position: "absolute",
    top: 180,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  topContent: {
    backgroundColor: primaryColor,
    height: 180,
    borderBottomRightRadius: 50,
  },
  bottomContent: {
    flex: 1,
    backgroundColor: secondaryColor,
    borderTopLeftRadius: 50
  },
  welcomeText: {
    marginTop: 30,
    marginBottom: 40,
    letterSpacing: .3,
    fontSize: 18,
    alignSelf: "stretch",
    textAlign: "center"
  },
  inputInline: {
    flexDirection: "row",
    alignItems: "center",
    width: 275,
    borderBottomColor: primaryColor,
    borderBottomWidth: 1,
    justifyContent: "space-between",
    marginBottom: 20
  },
  input: {
    flex: 1,
    height: 30,
    paddingHorizontal: 10,
  },
  btn: {
    borderRadius: 20,
    width: 175,
    paddingVertical: 10,
    alignItems: "center",
    backgroundColor: primaryColor,
    marginTop: 30
  },
  btnText : {
    fontSize: 16,
    fontWeight: "bold",
    letterSpacing: .3
  }
})

export default LoginScreen;