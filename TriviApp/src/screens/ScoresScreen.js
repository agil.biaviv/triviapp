import React, { useState } from 'react';
import { View, StyleSheet, StatusBar, Text, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector } from 'react-redux'

const ScoresScreen = (props) => {

  const triviaPoint = useSelector(state => state.triviaPoints)
  const animalPoints = useSelector(state => state.animalPoints)
  const computerPoints = useSelector(state => state.computerPoints)
  const mythPoints = useSelector(state => state.mythPoints)
  return (
    <>
    <StatusBar translucent={false}/>

    <View style={styles.bgTop} >
      <View style={styles.topContent}></View>
    </View>
    <View style={styles.bgBottom} >
      <View style={styles.bottomContent}></View>
    </View>

    <ScrollView style={styles.container}>

      <View style={styles.scoreBox}>
        <Text style={{fontWeight: "bold", fontSize: 18,}}>Your Trivia Points</Text>
        <Text style={{fontSize: 90, fontWeight: "bold", }}>{triviaPoint}</Text>
      </View>



      <View style={styles.detailList}>
        <Text style={{color: secondaryColor}}>Animals</Text>
        <Text style={{color: secondaryColor}}>{animalPoints}</Text>
      </View>

      <View style={styles.detailList}>
        <Text style={{color: secondaryColor}}>Computer Science</Text>
        <Text style={{color: secondaryColor}}>{computerPoints}</Text>
      </View>

      <View style={styles.detailList}>
        <Text style={{color: secondaryColor}}>Mythology</Text>
        <Text style={{color: secondaryColor}}>{mythPoints}</Text>
      </View>

    </ScrollView>
    </>
  );
}


let primaryColor = "black"
let secondaryColor = "white"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    position: "relative",
    zIndex: 0,
    paddingHorizontal: 30
  },
  bgTop: {
    backgroundColor: secondaryColor,
    height: 180,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  bgBottom: {
    backgroundColor: primaryColor,
    position: "absolute",
    top: 180,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  topContent: {
    backgroundColor: primaryColor,
    height: 180,
    borderBottomRightRadius: 50,
  },
  bottomContent: {
    flex: 1,
    backgroundColor: secondaryColor,
    borderTopLeftRadius: 50
  },
  scoreBox: {
    padding: 30,
    alignSelf: "stretch",
    marginTop: 30,
    marginBottom: 50,
    shadowColor: "#888",
    shadowOffset: {x: 0, y: 10},
    shadowRadius: 10,
    shadowOpacity: .3,
    elevation: 20,
    backgroundColor: secondaryColor,
    borderRadius: 40,
    alignItems: "center",
  },
  detailList: {
    alignSelf: "stretch",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: primaryColor,
    borderRadius: 20,
    marginBottom: 10
  }
})
export default ScoresScreen;