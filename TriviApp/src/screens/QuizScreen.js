import React, { useState, useEffect } from 'react';
import { View, Text, Button, StatusBar, StyleSheet, TouchableHighlight } from 'react-native';
import Question from '../components/question'
import Options from '../components/AnswerOptions'

import Axios from 'axios'
import { FlatList } from 'react-native-gesture-handler';

import { addAnimalPoints, addComputerPoints, addMythPoints } from '../store/actions'

import {useDispatch, useSelector} from 'react-redux'
import { mythPoints, animalPoints } from '../store/reducers/score';

const QuizScreen = (props) => {
  const id = props.route.params.categoryId
  // 18 computer, 20 myth, 27 animals

  const getCategory = (id) => {
    switch(id.toString()) {
      case '18' :
        return addComputerPoints
      case '20' :
        return addMythPoints
      case '27' :
        return addAnimalPoints
      default :
        return "Category ID is not match, your input :" + id
    }
  }
  const category = getCategory(id)

  const [data, setData] = useState({})
  const [answers, setAnswers] = useState()
  const [answerKey, setAnswerKey] = useState()

  const [isLoading, setIsLoading] = useState(true)


  const questionIndex = useSelector(state => state.questionNumber)
  const dispatch = useDispatch()

  useEffect(() => {
    const getQuestion = async() => {
      try{
        const response = await Axios.get(`https://opentdb.com/api.php?amount=10&category=${id}&type=multiple`)
        setData(response.data.results)
        setIsLoading(false)
      } catch {
        alert('error')
      }
    }
    if(data.length == undefined) {
      getQuestion()
    }
  }, [])

  useEffect(() => {
    if(data.length != undefined) {
      setAnswerKey(data[questionIndex].correct_answer)
      setAnswers([...data[questionIndex].incorrect_answers, data[questionIndex].correct_answer])
    }
  }, [isLoading])

  const nextHandler = (index) => {
    setAnswerKey(data[index+ 1].correct_answer)
    setAnswers([...data[index + 1].incorrect_answers, data[index + 1].correct_answer])
  }

  return (
    !isLoading ?
    (<>
      <StatusBar translucent={false}/>
      <View style={styles.bgTop} >
        <View style={styles.topContent}></View>
      </View>
      <View style={styles.bgBottom} >
        <View style={styles.bottomContent}></View>
      </View>

      <View style={styles.container}>
        <View style={styles.navbar}>
          <Text style={styles.counter}> {Number(questionIndex) + 1} / 10</Text>
        </View>
        <View style={styles.questionBox}>
          <Question data={data[questionIndex]} />
        </View>

        <View style={styles.optionsBox}>
          <FlatList
            data={answers}
            keyExtractor={(data, index) => index.toString()}
            renderItem={({ item, index }) => ( <Options data={item}
                                                  answerKey={answerKey}
                                                  navigation={props.navigation}
                                                  nextHandler={nextHandler}
                                                  category={category}
                                                  />) }
          />
        </View>
      </View>
    </>) : (<View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
              <Text style={{fontSize: 18, fontWeight: "bold", letterSpacing: 1}}>{'Loading'}</Text>
            </View>)
  );
}





const primaryColor = "black"
const secondaryColor = "white"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    position: "relative",
    zIndex: 0,
  },
  bgTop: {
    backgroundColor: secondaryColor,
    height: 180,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  bgBottom: {
    backgroundColor: primaryColor,
    position: "absolute",
    top: 180,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: -1,
  },
  topContent: {
    backgroundColor: primaryColor,
    height: 180,
    borderBottomRightRadius: 50,
  },
  bottomContent: {
    flex: 1,
    backgroundColor: secondaryColor,
    borderTopLeftRadius: 50
  },
  navbar: {
    padding: 20,
  },
  counter: {
    color: secondaryColor,
    textAlign: "right",
    fontWeight: "bold",
    fontSize: 16
  },
  questionBox: {
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  question: {
    color: secondaryColor,
    textAlign: "center",
    fontSize: 14
  },
  optionsBox: {
    flex: 1,
    marginTop: 100
  }

})

export default QuizScreen;