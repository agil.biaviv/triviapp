import { StatusBar } from 'expo-status-bar';
import React from 'react';
import MyApp from './src'

export default function App() {
  return (
    <MyApp />
  );
}

